# A self-contained utility command library for Jim Tcl that fills some gaps.
# ==============================================================================
# This code with the exception of the following procedures
# * [lib::commands]
# * [lib::try]
# is copyright (c) 2019, 2020 D. Bohdan and contributors listed in AUTHORS and
# is released under the MIT license (below).
# ==============================================================================
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# ==============================================================================
# The procedure [lib::commands] is copyright (c) 2013-2019 Poor Yorick and is
# derived from the original 1998 implementation of [cmdSplit] by
# Donald G. Porter, which is in the public domain as a US government work.
# It is released under the Tcl license (below).
# ==============================================================================
# This software is copyrighted by the Regents of the University of
# California, Sun Microsystems, Inc., Scriptics Corporation, ActiveState
# Corporation and other parties.  The following terms apply to all files
# associated with the software unless explicitly disclaimed in
# individual files.
#
# The authors hereby grant permission to use, copy, modify, distribute,
# and license this software and its documentation for any purpose, provided
# that existing copyright notices are retained in all copies and that this
# notice is included verbatim in any distributions. No written agreement,
# license, or royalty fee is required for any of the authorized uses.
# Modifications to this software may be copyrighted by their authors
# and need not follow the licensing terms described here, provided that
# the new terms are clearly indicated on the first page of each file where
# they apply.
#
# IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
# DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
# IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
# NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
# MODIFICATIONS.
#
# GOVERNMENT USE: If you are acquiring this software on behalf of the
# U.S. government, the Government shall have only "Restricted Rights"
# in the software and related documentation as defined in the Federal
# Acquisition Regulations (FARs) in Clause 52.227.19 (c) (2).  If you
# are acquiring the software on behalf of the Department of Defense, the
# software shall be classified as "Commercial Computer Software" and the
# Government shall have only "Restricted Rights" as defined in Clause
# 252.227-7014 (b) (3) of DFARs.  Notwithstanding the foregoing, the
# authors grant the U.S. Government and others acting in its behalf
# permission to use and distribute the software in accordance with the
# terms specified in this license.
# ==============================================================================
# The procedure [lib::try] is adapted from Tcllib 1.19 code that carries the
# following copyright notice.
# ==============================================================================
# (C) 2008-2011 Donal K. Fellows, Andreas Kupries, BSD licensed.
# ==============================================================================

namespace eval lib {
    variable version 0.6.0
}


proc lib::$ {varName args} {
    upvar 1 $varName v
    if {![exists v]} {
        return -code error \
               -errorcode {JIMLIB $ NO-SUCH-VARIABLE} \
               "can't read \"$varName\": no such variable"
    }

    if {[llength $args] == 0} {
        return $v
    } else {
        return [dict get $v {*}$args]
    }
}


proc lib::checksum::parse::many text {
    concat {*}[lmap line [split $text \n] {
        if {[regexp {^\s*$} $line]} continue

        set line [string trimright $line \r]

        try {
            tagged-line $line
        } on error _ {
            try {
                line $line
            }
        }
    }]
}


proc lib::checksum::parse::line line {
    if {![regexp {^(\\?)([0-9a-fA-Z]+)\s+\*?(.*)$} $line _ slash sum path]} {
        return -code error \
               -errorcode {JIMLIB CHECKSUM PARSE LINE} \
               [list can't parse line $line]
    }

    if {$slash eq "\\"} {
        set path [subst -nocommands -novariables $path]
    }

    list ? $path $sum
}


proc lib::checksum::parse::tagged-line line {
    if {![regexp -indices {^(\\?)([^\s]+)\s+\(} $line \
                          beforeInd slashInd algoInd]} {
        return -code error \
               -errorcode {JIMLIB CHECKSUM PARSE TAGGED-LINE} \
               [list can't parse line $line]
    }

    set algo [string range $line {*}$algoInd]

    if {![regexp -indices {\)\s+=\s+([0-9a-fA-Z]+)$} $line \
                          afterInd sumInd]} {
        return -code error \
               -errorcode {JIMLIB CHECKSUM PARSE TAGGED-LINE} \
               [list can't parse line $line]
    }

    set sum [string range $line {*}$sumInd]
    set path [string range $line \
                           [lindex $beforeInd 1]+1 \
                           [lindex $afterInd 0]-1]

    if {$slashInd eq {0 0}} {
        set path [subst -nocommands -novariables $path]
    }

    list $algo $path $sum
}


# A rough implementation of [clock add].  Doesn't understand time zones, leap
# seconds, and other nuance.
proc lib::clock::add {clockval args} {
    foreach {amount unit} $args {
        set mul 0

        switch $unit {
            s -
            second -
            seconds {
                set mul 1
            }

            m -
            min -
            mins -
            minute -
            minutes {
                set mul 60
            }

            h -
            hr -
            hrs -
            hour -
            hours {
                set mul $(60 * 60)
            }

            d -
            day -
            days {
                set mul $(60 * 60 * 24)
            }

            w -
            wk -
            wks -
            week -
            weeks {
                set mul $(60 * 60 * 24 * 7)
            }

            mo -
            mos -
            mon -
            mons -
            month -
            months {
                set clockval [add-months $clockval $amount]
            }

            y -
            yr -
            yrs -
            year -
            years {
                set clockval [add-months $clockval $(12 * $amount)]
            }

            default {
                return -code error \
                       -errorcode {JIMLIB CLOCK ADD} \
                       [list unknown time unit $unit]
            }
        }

        set clockval $($clockval + $amount * $mul)
    }

    return $clockval
}


proc lib::clock::add-months {clockval n} {
    set daysInMonth {xx 31 2x 31 30 31 30 31 31 30 31 30 31}

    for {set i 0} {$i < $n} {incr i} {
        lassign [clock format $clockval -format {%Y %m} -gmt 1] year month
        set leap [leap-year? $year]
        lset daysInMonth 2 $($leap ? 29 : 28)

        set clockval $(
            $clockval + [lindex $daysInMonth $month] * 24 * 60 * 60
        )
    }

    return $clockval
}


proc lib::clock::leap-year? year {
    if {$year % 4 != 0} {
        return 0
    }

    if {$year % 100 != 0} {
        return 1
    }

    if {$year % 400 != 0} {
        return 0
    }

    return 1
}


proc lib::clock::scan-incremental {date {options {}} {debug 0}} {
    set date [regsub -all {[ :.T/]+} $date {-}]

    set resultTimeVal {}
    set resultFormat {}
    foreach {formatScan formatStandard padding} {
        {%Y}                {%Y}                {-01-01-00-00-00}
        {%Y-%m}             {%Y-%m}             {-01-00-00-00}
        {%Y-%m-%d}          {%Y-%m-%d}          {-00-00-00}
        {%Y-%m-%d-%H-%M}    {%Y-%m-%dT%H:%M}    {-00}
        {%Y-%m-%d-%H-%M-%S} {%Y-%m-%dT%H:%M:%S} {}
    } {
        if {$debug} {
            log debug [list $formatScan $date]
        }
        try {
            clock scan $date -format $formatScan {*}$options
        } on ok scan {
            # Work around unexpected treatment of %Y and %Y-%m dates;
            # see http://wiki.tcl-lang.org/2525.
            set resultTimeVal [clock scan [join [list $date $padding] ""] \
                    -format {%Y-%m-%d-%H-%M-%S} {*}$options]
            set resultFormat $formatStandard
            if {$debug} {
                log debug match
                log debug [clock format $scan {*}$options]
            }
        } on error {} {}
    }
    return [list $resultTimeVal $resultFormat]
}


proc lib::configure {} {
    upvar 1 args args \
            config config

    if {[lindex $args 0] eq {-configure}} {
        set args [lassign $args _ configUpdate]

        set unknownKeys [lsort [set::diff [dict keys $configUpdate] \
                                          [dict keys $config]]]
        if {$unknownKeys ne {}} {
            return -code error \
                   -errorcode {JIMLIB CONFIGURE UNKNOWN-KEYS} \
                   [list unknown config keys $unknownKeys]
        }

        set config [dict merge $config $configUpdate]
    }
}


# By Donald G. Porter and Poor Yorick (https://wiki.tcl-lang.org/page/cmdSplit).
# Modified to return $report.
proc lib::commands {script {returnReport 0}} {
    set report {}
    set commands {}
    set command {}
    set comment 0
    set lineidx 0
    set offset 0

    foreach line [split $script \n] {
        set parts [split $line \;]
        set numparts [llength $parts]
        set partidx 0
        while 1 {
            set parts [lassign $parts[set parts {}] part]
            if {[string length $command]} {
                if {$partidx} {
                    append command \;$part
                } else {
                    append command \n$part
                }
            } else {
                set partlength [string length $part]
                set command [string trimleft $part[set part {}] "\f\n\r\t\v "]
                incr offset [expr {$partlength - [string length $command]}]
                if {[string match #* $command]} {
                    set comment 1
                }
            }

            if {$command eq {}} {
                incr offset
            } elseif {(!$comment || (
                    $comment && (!$numparts || ![llength $parts])))
                && [info complete $command\n]} {

                lappend commands $command
                set info [dict create character $offset line $lineidx]
                set offset [expr {$offset + [string length $command] + 1}]
                lappend report $info
                set command {}
                set comment 0
                set info {}
            }

            incr partidx
            if {![llength $parts]} break
        }
    }

    incr lineidx

    if {$command ne {}} {
        return -code error \
               -errorcode {JIMLIB COMMANDS INCOMPLETE} \
               [list {incomplete command} $command]
    }

    if {$returnReport} {
        return [list $commands $report]
    } else {
        return $commands
    }
}


proc lib::csv::split {line {seps ,}} {
    if {$line eq {}} {
        return {}
    }

    set field {}
    set fields {}
    set prev {}
    set quoted 0

    foreach c [::split $line {}] {
        if {$c eq {"}} {
            if {$prev eq {"}} {
                append field \"
            }

            set quoted $(!$quoted)
        } elseif {!$quoted && $c in $seps} {
            lappend fields $field
            set field {}
        } else {
            append field $c
        }

        set prev $c
    }

    lappend fields $field

    return $fields
}


# Validate and rename a dictionary's keys.
proc lib::dict::map {mapping srcDict} {
    set destDict {}

    dict for {srcKey dest} $mapping {
        catch {unset default}

        switch [llength $dest] {
            1 { set destKey $dest }
            2 { lassign $dest destKey default }
            default {
                return -code error \
                       -errorcode {JIMLIB DICT MAP BAD-MAPPING} \
                       [list expected {destKey ?default?} but got $dest] }
        }

        if {[info exists srcDict($srcKey)]} {
            set destDict($destKey) $srcDict($srcKey)
        } elseif {[info exists default]} {
            set destDict($destKey) $default
        } else {
            return -code error \
                   -errorcode {JIMLIB DICT MAP MISSING-KEY} \
                   [list missing required key $srcKey]
        }

        dict unset srcDict $srcKey
    }

    if {$srcDict ne {}} {
        return -code error \
               -errorcode {JIMLIB DICT MAP EXTRA-KEYS} \
               [list unknown extra keys: $srcDict]
    }

    return $destDict
}


proc lib::dict::fill-repeats dict {
    if {[llength $dict] % 2 == 1} {
        return -code error \
               -errorcode {JIMLIB DICT FILL-REPEATS NOT-A-DICTIONARY} \
               [list not a dictionary: $dict]
    }

    set prevV {}
    foreach {v k} [lreverse $dict] {
        if {$v eq {-}} {
            dict set dict $k $prevV
        } else {
            set prevV $v
        }
    }

    return $dict
}


proc lib::file::find {
    {startDir .}
    {fileVarName file}
    {depthVarName depth}
    {fileFilter {}}
    {fileScript {}}
    {dirVarName dir}
    {dirFilter {}}
} {
    set found {}
    set queue [list $startDir 0]

    upvar 1 $dirVarName dir \
            $fileVarName file \
            $depthVarName depth

    while {$queue ne {}} {
        set queue [lassign $queue dir depth]

        if {$dirFilter ne {} && ![uplevel 1 [list expr $dirFilter]]} {
            continue
        }

        incr depth

        foreach file [list-dir $dir] {
            if {[file isdir $file]} {
                lappend queue $file $depth
                continue
            }

            if {$fileFilter ne {} && ![uplevel 1 [list expr $fileFilter]]} {
                continue
            }

            if {$fileScript eq {}} {
                lappend found $file
            } else {
                set result [uplevel 1 $fileScript]
                # Valid filenames aren't be empty.
                if {$result ne {}} {
                    lappend found $result
                }
            }
        }
    }

    return $found
}


proc lib::file::list-dir {args dir} {
    set tails 0
    set switches [list -directory $dir -nocomplain]
    while {$args ne {}} {
        set args [lassign $args arg]
        switch -- $arg {
            -tails {
                set tails 1
            }
            default {
                return -code error \
                       -errorcode {JIMLIB FILE LIST-DIR UNKNOWN-SWITCH} \
                       [list unknown switch $arg]
            }
        }
    }

    set dots [lmap file [glob -tails {*}$switches .*] {
        if {$file in {. ..}} continue

        expr {$tails ? $file : [file join $dir $file]}
    }]

    set normal [glob {*}$($tails ? {-tails} : {}) {*}$switches *]

    set files [lsort [concat $dots $normal]]

    return $files
}


proc lib::file::read path {
    set ch [open $path rb]
    defer {close $ch}

    return [::read $ch]
}


proc lib::file::reroot {fromDir toDir path} {
    set pathSplit [split $path]
    set fromSplit [split $fromDir]
    set fromLen [llength $fromSplit]

    if {[lrange $pathSplit 0 $fromLen-1] ne $fromSplit} {
        return -code error \
               -errorcode {JIMLIB FILE REROOT BAD-FROMDIR} \
               [list $path not in $fromDir]
    }

    return [file join $toDir {*}[lrange $pathSplit $fromLen end]]
}


set lib::file::run::config {
    script lindex
}


proc lib::file::run args {
    variable run::config

    if {$args eq {}} return

    {*}$config(script) $args
    exec {*}$args >@ stdout 2>@stderr
}


# This command doesn't understand Windows volumes and paths or UNC.
proc lib::file::split {path {max 1024}} {
    # Detect and strip leading slashes.
    set absolute [regexp ^/+(.*)$ $path _ path]
    set head [expr {!$absolute}]

    set parts {}

    # We can't use [regexp -indices -start N] here because it will cause
    # problems with Unicode in Jim Tcl 0.78 or earlier.
    while {[regexp {^([^/]+)/+(.*)$} $path _ part path]} {
        if {!$head && [regexp ^~ $part]} {
            set part ./$part
        }
        lappend parts $part

        if {[llength $parts] > $max} {
            return -code error \
                   -errorcode {JIMLIB FILE SPLIT TOO-MANY-PARTS} \
                   {aborted: path has too many parts}
        }

        set head 0
    }

    if {$path ne {}} {
        if {!$head && [regexp ^~ $path]} {
            set path ./$path
        }
        lappend parts $path
    }

    if {$absolute} {
        set parts [linsert $parts 0 /]
    }

    return $parts
}


proc lib::file::tar::finish ch {
    $ch puts -nonewline [string repeat \0 1024]
}


proc lib::file::tar::header args {
    lib::named-args {
        -filename filename
        -mode {mode 420}
        -uid {uid 0}
        -gid {gid 0}
        -size size
        -mtime {mtime 0}
        -type {type 0}
        -linkname {linkname {}}
        -uname {uname root}
        -gname {gname root}
        -devmajor {devmajor 0}
        -devminor {devminor 0}
        -prefix {prefix {}}
    }

    set formats {}
    set values {}

    local proc field {name max value {null true}} {
        upvar 1 formats formats
        upvar 1 values values

        set null $($null ? 1 : 0)

        set bytes [string bytelength $value]
        if {$bytes > $max - $null} {
            return -code error \
                   -errorcode {JIMLIB FILE TAR HEADER FIELD-TOO-LONG} \
                   [list field $name exceeds $($max - $null) bytes]
        }

        append formats a$max
        lappend values $value
    }

    field filename 100 $filename
    field mode     8   [format %07o $mode]
    field uid      8   [format %07o $uid]
    field gid      8   [format %07o $gid]
    field size     12  [format %011o $size]
    field mtime    12  [format %011o $mtime]
    field checksum 8   {        }            false  ;# Checksum.
    field type     1   [format %1o $type]    false
    field linkname 100 $linkname

    field magic    6   ustar
    field version  2   00                    false  ;# Version.
    field uname    32  $uname
    field gname    32  $gname
    field devmajor 8   [format %07o $devmajor]
    field devminor 8   [format %07o $devminor]
    field prefix   155 $prefix  ;# Filename prefix.

    set checksum 0
    foreach value $values {
        binary scan $value cu* bytes
        foreach b $bytes {
            incr checksum $b
        }
    }
    lset values 6 [format %07o $checksum]

    set header [binary format $formats {*}$values]
}


proc lib::file::tar::write {ch data args} {
    set size [string bytelength $data]
    set header [header {*}$args -size $size]
    $ch puts -nonewline $header
    $ch puts -nonewline \0\0\0\0\0\0\0\0\0\0\0\0
    $ch puts -nonewline $data
    if {$size % 512 != 0} {
        $ch puts -nonewline [string repeat \0 $(512 - $size % 512)]
    }
}


proc lib::file::with-path {path script} {
    set prev [pwd]
    try {
        cd $path
        uplevel 1 $script
    } finally {
        cd $prev
    }
}


proc lib::file::write {path contents} {
    set ch [open $path wb]
    defer {close $ch}

    puts -nonewline $ch $contents
}


proc lib::hash::mult {s {init 5381} {m 33}} {
    set hash $init
    for {set i 0} {$i < [string bytelength $s]} {incr i} {
        set c [string byterange $s $i $i]
        set hash $($m * $hash + [scan $c %c])
    }
    return $hash
}


proc lib::id value {
    return $value
}


proc lib::inspect {label args} {
    # Prevent name clashes.  We let the caller name the variable for [parray].
    if {$label in {__label __procName}} {
        return -code error \
               -errorcode {JIMLIB INSPECT BAD-LABEL} \
               [list invalid label: $label]
    }

    set data {}
    foreach varName $args {
        upvar 1 $varName v
        if {[info exists v]} {
            set data($varName) $v
            continue
        }

        set data($varName) {variable does not exist}
    }

    set __procName ::inspect-$label-puts-stderr
    # Can't use a lambda or a script with [parray].
    local proc $__procName s {
        puts stderr $s
    }

    set __label $label
    set $__label $data

    parray $__label * $__procName
}


proc lib::list::diff {before after} {
    lassign [lcs $before $after] lcs1 lcs2

    set diff {}

    if {$lcs1 eq {}} {
        foreach el $before {
            lappend diff - $el
        }

        foreach el $after {
            lappend diff + $el
        }

        return $diff
    }

    local proc diff-add {symbol source prev current} {
        upvar 1 diff diff

        for {set i $($prev + 1)} {$i < $current} {incr i} {
            lappend diff $symbol [lindex $source $i]
        }
    }

    set prevI -1
    set prevJ -1
    foreach i $lcs1 j $lcs2 {
        diff-add - $before $prevI $i
        diff-add + $after $prevJ $j

        lappend diff = [lindex $before $i]

        set prevI $i
        set prevJ $j
    }
    diff-add - $before $prevI [llength $before]
    diff-add + $after $prevJ [llength $after]

    return $diff
}


# Return one longest common subsequence of $list1 and $list2.
proc lib::list::lcs {list1 list2} {
    set m [lcs::length $list1 $list2]
    return [lcs::backtrack $list1 $list2 $m [llength $list1] [llength $list2]]
}


proc lib::list::lcs::backtrack {list1 list2 m i j} {
    if {$i == 0 || $j == 0} {
        return {{} {}}
    }

    if {[lindex $list1 $i-1] eq [lindex $list2 $j-1]} {
        lassign [backtrack $list1 $list2 $m $($i - 1) $($j - 1)] bt1 bt2
        return [list [concat $bt1 $($i - 1)] [concat $bt2 $($j - 1)]]
    }

    if {$m($i $($j - 1)) > $m($($i - 1) $j)} {
        tailcall backtrack $list1 $list2 $m $i $($j - 1)
    }

    tailcall backtrack $list1 $list2 $m $($i - 1) $j
}


proc lib::list::lcs::length {list1 list2} {
    set len1 [llength $list1]
    set len2 [llength $list2]

    for {set i 0} {$i <= $len1} {incr i} {
        set "m($i 0)" 0
    }
    for {set j 0} {$j <= $len2} {incr j} {
        set "m(0 $j)" 0
    }

    for {set i 1} {$i <= $len1} {incr i} {
        for {set j 1} {$j <= $len2} {incr j} {
            if {[lindex $list1 $i-1] eq [lindex $list2 $j-1]} {
                set "m($i $j)" $(1 + $m($($i - 1) $($j - 1)))
            } else {
                set "m($i $j)" [lib::math::max $m($($i - 1) $j) \
                                               $m($i $($j - 1))]
            }
        }
    }

    return $m
}


proc lib::list::pick {list indices} {
    set res {}

    foreach i $indices {
        lappend res [lindex $list $i]
    }

    return $res
}


proc lib::list::sl {script {prefix list}} {
    set list [lmap command [lib::commands $script] {
        if {[string index $command 0] eq {#}} continue
        uplevel 1 {*}$prefix $command
    }]

    return [concat {*}$list]
}


set lib::log::config {
    channel stderr
    format {== $timestamp ($level) [list $message]}
    gmt 1
    level info
    levels {emerg alert crit err warning notice info debug discard}
    timestamp {%Y-%m-%d %H:%M:%S UTC}
}


proc lib::log {level message} {} {
    variable log::config

    local proc levelNumber level config {
        set i [lsearch $config(levels) $level]
        if {$i == -1} {
            return -code error \
                   -errorcode {JIMLIB LOG BAD-LEVEL} \
                   [list unknown log level $level]
        }

        return $i
    }

    if {[levelNumber $config(level)] < [levelNumber $level]} {
        return
    }

    set timestamp [clock format [clock seconds] \
                                -format $config(timestamp) \
                                -gmt $config(gmt)]
    puts $config(channel) [subst $config(format)]
}


namespace eval lib::math {
    variable pi 3.14159265358979323846
    variable halfPi [expr {$pi / 2}]
}


proc lib::math::atan2 {y x} {
    variable pi
    variable halfPi

    if {$x > 0} {
        return $(atan($y / $x))
    } elseif {$x < 0 && $y >= 0} {
        return $(atan($y / $x) + $pi)
    } elseif {$x < 0 && $y < 0} {
        return $(atan($y / $x) - $pi)
    } elseif {$x == 0 && $y > 0} {
        return $halfPi
    } elseif {$x == 0 && $y < 0} {
        return -$halfPi
    } else {
        # By convention.
        return 0.0
    }
}


proc lib::math::complex::+ {z1 z2} {
    lassign $z1 a1 b1
    lassign $z2 a2 b2
    if {$b1 eq {}} { set b1 0 }
    if {$b2 eq {}} { set b2 0 }
    return [list $($a1 + $a2) $($b1 + $b2)]
}


proc lib::math::complex::* {z1 z2} {
    lassign $z1 a1 b1
    lassign $z2 a2 b2
    if {$b1 eq {}} { set b1 0 }
    if {$b2 eq {}} { set b2 0 }
    return [list $($a1*$a2 - $b1*$b2) $($a1*$b2 + $a2*$b1)]
}


proc lib::math::complex::mod z {
    lassign $z a b
    if {$b eq {}} { set b 0 }
    return $(sqrt($a*$a + $b*$b))
}


proc lib::math::max args {
    return [lindex [lsort -real $args] end]
}


proc lib::math::min args {
    return [lindex [lsort -real $args] 0]
}


proc lib::mp::pmap {varList list body chunkSize} {
    set tasks {}
    for {set start 0} {$start < [llength $list]} {incr start $chunkSize} {
        set end $($start + $chunkSize - 1)
        set chunk [lrange $list $start $end]
        set cmd [list \
            | [info nameofexecutable] \
              -e "lmap [list $varList] \[gets stdin\] [list $body]" \
              << $chunk \
              2>@1 \
        ]
        lappend tasks [open $cmd] $start $end
    }

    set results [lmap {ch start end} $tasks {
        set output [gets $ch]

        set pid [pid $ch]
        lassign [wait $pid] _ _ status
        if {$status != 0} {
            return -code error \
                   -errorcode [list JIMLIB MP PMAP CHILDSTATUS $status] \
                   [list child process $pid for chunk $start to $end exited \
                         with status $status and message $output]
        }

        lindex $output
    }]

    return [concat {*}$results]
}


proc lib::named-args {spec {positional 0}} {
    upvar 1 args args

    lassign [lib::named-args::parse $spec $positional $args] vars args
    foreach {k v} $vars {
        uplevel 1 [list set $k $v]
    }

    return $args
}


proc lib::named-args::parse {spec positional arguments} {
    set spec [lib::dict::fill-repeats $spec]

    set missingVars {}
    set parsed {}
    foreach {flag v} $spec {
        lassign $v varName default options

        if {[llength $v] > 1} {
            dict set parsed $varName $default
        }

        if {[llength $v] == 1 || {required} in $options} {
            dict lappend missingVars $varName $flag
        }
    }

    set unknownArgs {}

    while {$arguments ne {}} {
        set arg [lindex $arguments 0]
        if {$arg eq {--} || ![string match -* $arg]} break
        set arguments [lrange $arguments 1 end]

        if {[dict exists $spec $arg]} {
            catch {unset varName default}
            set options {}

            lassign [dict get $spec $arg] varName default options

            dict unset missingVars $varName

            if {{flag} in $options} {
                continue
            }

            set arguments [lassign $arguments value]
            dict set parsed $varName $value

            continue
        }

        lappend unknownArgs $arg
    }

    if {$missingVars ne {}} {
        return -code error \
               -errorcode {JIMLIB NAMED-ARGS PARSE MISSING} \
               [list missing required argument {*}[dict values $missingVars]]
    }

    if {!$positional} {
        lappend unknownArgs {*}$arguments
    }

    if {$unknownArgs ne {}} {
        return -code error \
               -errorcode {JIMLIB NAMED-ARGS PARSE EXTRA} \
               [list unknown extra arguments: $unknownArgs]
    }

    return [list $parsed $arguments]
}


proc lib::net::curl args {
    try {
        exec curl \
            --fail \
            --location \
            --show-error \
            --silent \
            {*}$args \
            2>@1
    } on error {result opts} {
        set errorcode {JIMLIB NET CURL}
        if {[regexp {returned error: ([0-9]+)} $result _ httpError]} {
            lappend errorcode HTTP $httpError
        }
        return -code error \
               -errorcode $errorcode \
               [lindex [split $result \n] 0]
    }
}


proc lib::pipe {varName initial args} {
    upvar 1 $varName v
    set v $initial

    set mode {}
    while {$args ne {}} {
        set args [lassign $args arg]

        switch -- $arg {
            -all -
            -apply {
                set mode APPLY
                continue
            }

            -each -
            -map {
                set mode MAP
                continue
            }

            -each* -
            -flatmap -
            -flatMap -
            -map* {
                set mode FLAT_MAP
                continue
            }

            -filter {
                set mode FILTER
                continue
            }

            -fold -
            -reduce {
                set mode REDUCE

                set args [lassign $args accVarName accInitial]
                upvar 1 $accVarName acc
                set acc $accInitial

                continue
            }
        }

        switch $mode {
            FILTER {
                set script "
                    if {!\[[list expr $arg]\]} continue
                    lindex $[list $varName]
                "
                set v [uplevel 1 [list lmap $varName $v $script]]
            }

            FLAT_MAP -
            MAP {
                set v [uplevel 1 [list lmap $varName $v $arg]]

                if {$mode eq {FLAT_MAP}} {
                    set v [concat {*}$v]
                }
            }

            REDUCE {
                set items $v
                foreach v $items {
                    set acc [uplevel 1 $arg]
                }

                set v $acc
            }

            default {
                set v [uplevel 1 $arg]
            }
        }

        set mode {}
    }

    return $v
}


proc lib::set::diff {a b} {
    set diff {}

    foreach x $a {
        if {$x ni $b} {
            lappend diff $x
        }
    }

    return $diff
}


proc lib::set::subset? {a b} {
    foreach x $a {
        if {$x ni $b} {
            return 0
        }
    }

    return 1
}


proc lib::set::sym-diff {a b} {
    set inters {}
    set symDiff {}

    foreach x $a {
        if {$x in $b} {
            lappend inters $x
        } else {
            lappend symDiff $x
        }
    }

    foreach x $b {
        if {$x ni $inters} {
            lappend symDiff $x
        }
    }

    return [list $symDiff $inters]
}


# A slow Unicode-agnostic [string first].
proc lib::string::bytefirst {needle haystack} {
    set bytesNeedle [string bytelength $needle]
    set bytesHaystack [string bytelength $haystack]

    set n $($bytesHaystack - $bytesNeedle)
    for {set i 0} {$i <= $n} {incr i} {
        set range [string byterange $haystack $i $($i + $bytesNeedle - 1)]
        if {$range eq $needle} {
            return $i
        }
    }

    return -1
}


proc lib::string::indent {string prefix} {
    set lines [lmap line [split $string \n] { lindex $prefix$line }]
    return [join $lines \n]
}


proc lib::string::match-bracket {args s} {
    lib::named-args {
        -opening {opening (}
        -closing {closing )}
        -escape {escape \\}
        -start {start 0}
        -n {n 0}
    }

    if {[string index $s $start] ne $opening} {
        return -code error \
               -errorcode {JIMLIB STRING MATCH-BRACKET BAD-START} \
               {start isn't a bracket}
    }

    for {set i $start} {$i < [string length $s]} {incr i} {
        set c [string index $s $i]

        if {$c eq $escape} {
            incr i
            continue
        }

        if {$c eq $opening} {
            incr n
        } elseif {$c eq $closing} {
            incr n -1
        }

        if {$n == 0} break
    }

    if {$n > 0} {
        return -code error \
               -errorcode {JIMLIB STRING MATCH-BRACKET UNMATCHED} \
               [list unmatched brackets: $n]
    }

    return $i
}


# Split $str on separators that match $regexp. Returns a list consisting of
# fields and, if $includeSeparators is 1, the separators after each.
proc lib::string::sepsplit {str regexp {includeSeparators 1}} {
    if {$str eq {}} {
        return {}
    }
    if {$regexp eq {}} {
        return [split $str {}]
    }
    # Thanks to KBK for the idea.
    if {[regexp $regexp {}]} {
        return -code error \
               -errorcode {JIMLIB STRING SEPSPLIT INFINITE-LOOP}
               [list splitting on regexp $regexp would cause infinite loop]
    }

    # Split $str into a list of fields and separators.
    set fieldsAndSeps {}
    set offset 0
    while {[regexp -start $offset -indices -- $regexp $str match]} {
        lassign $match matchStart matchEnd
        lappend fieldsAndSeps \
                [string range $str $offset [expr {$matchStart - 1}]]
        if {$includeSeparators} {
            lappend fieldsAndSeps \
                    [string range $str $matchStart $matchEnd]
        }
        set offset [expr {$matchEnd + 1}]
    }
    # Handle the remainder of $str after all the separators.
    set tail [string range $str $offset end]
    if {$tail eq {}} {
        # $str ended on a separator.
        if {!$includeSeparators} {
            lappend fieldsAndSeps {}
        }
    } else {
        lappend fieldsAndSeps $tail
        if {$includeSeparators} {
            lappend fieldsAndSeps {}
        }
    }

    return $fieldsAndSeps
}


proc lib::string::slugify text {
    string trim [regsub -all {[^[:alnum:]]+} [string tolower $text] -] -
}


proc lib::template::eval {template args} {
    set code {}

    dict for {varName value} $args {
        append code [list set $varName $value]\n
    }
    append code [parse $template]

    return [uplevel 1 [list apply [list {} $code]]]
}


# Convert a template into Tcl code.
proc lib::template::parse {template} {
    set result {}
    set regExpr {^(.*?)<%(.*?)%>(.*)$}
    set listing "set _output {}\n"
    while {[regexp $regExpr $template match preceding token template]} {
        append listing [list append _output $preceding]\n

        switch -exact -- [string index $token 0] {
            = {
                append listing \
                       [format {append _output [expr %s]} \
                               [list [string range $token 1 end]]]
            }
            ! {
                append listing \
                       [format {append _output [%s]} \
                               [string range $token 1 end]]
            }
            default {
                append listing $token
            }
        }
        append listing \n
    }

    append listing [list append _output $template]\n

    return $listing
}


proc lib::term::ansi::sgr args {
    set attrs {}
    foreach call $args {
        set cmd sgr
        for {set i 0} {$i < [llength $call]} {incr i} {
            if {[string is integer [lindex $call $i]]} break
            append cmd ::[lindex $call $i]
        }

        lappend attrs {*}[$cmd {*}[lrange $call $i end]]
    }

    return [sgr::format $attrs]
}

proc lib::term::ansi::sgr::format attrs {
    return \033\[[join $attrs \;]m
}

proc lib::term::ansi::sgr::rgb-to-8-bit {r g b} {
    tailcall rgb5-to-8-bit $($r * 6 / 256) $($g * 6 / 256) $($b * 6 / 256)
}

proc lib::term::ansi::sgr::rgb5-to-8-bit {r g b} {
    if {!(   0 <= $r && $r <= 5
          && 0 <= $g && $g <= 5
          && 0 <= $b && $b <= 5)} {
        return -code error \
               -errorcode {JIMLIB TERM ANSI SGR RGB5-TO-8-BIT OUT-OF-RANGE} \
               [list rgb values [list $r $g $b] not in range of 0 to 5]
    }
    return $(16 + 36*$r + 6*$g + $b)
}

proc lib::term::ansi::sgr::bold {} { return 1}
proc lib::term::ansi::sgr::bold::off {} { return 22 }
proc lib::term::ansi::sgr::faint {} { return 2 }
proc lib::term::ansi::sgr::faint::off {} { return 22 }
proc lib::term::ansi::sgr::italic {} { return 3 }
proc lib::term::ansi::sgr::italic::off {} { return 23 }
proc lib::term::ansi::sgr::underline {} { return 4 }
proc lib::term::ansi::sgr::underline::double {} { return 21 }
proc lib::term::ansi::sgr::underline::off {} { return 24 }
proc lib::term::ansi::sgr::blink {} { return 5 }
proc lib::term::ansi::sgr::blink::fast {} { return 6 }
proc lib::term::ansi::sgr::blink::off {} { return 25 }
proc lib::term::ansi::sgr::reverse {} { return 7 }
proc lib::term::ansi::sgr::reverse::off {} { return 27 }
proc lib::term::ansi::sgr::conceal {} { return 8 }
proc lib::term::ansi::sgr::conceal::off {} { return 28 }
proc lib::term::ansi::sgr::strikethrough {} { return 9 }
proc lib::term::ansi::sgr::strikethrough::off {} { return 29 }
proc lib::term::ansi::sgr::overline {} { return 53 }
proc lib::term::ansi::sgr::overline::off {} { return 55 }

proc lib::term::ansi::sgr::custom args { return $args }

proc lib::term::ansi::sgr::fg::black {} { return 30 }
proc lib::term::ansi::sgr::fg::red {} { return 31 }
proc lib::term::ansi::sgr::fg::green {} { return 32 }
proc lib::term::ansi::sgr::fg::yellow {} { return 33 }
proc lib::term::ansi::sgr::fg::blue {} { return 34 }
proc lib::term::ansi::sgr::fg::magenta {} { return 35 }
proc lib::term::ansi::sgr::fg::cyan {} { return 36 }
proc lib::term::ansi::sgr::fg::white {} { return 37 }
proc lib::term::ansi::sgr::fg::8-bit color { return [list 38 5 $color] }
proc lib::term::ansi::sgr::fg::rgb {r g b} { return [list 38 2 $r $g $b] }
proc lib::term::ansi::sgr::fg::default {} { return 39 }

proc lib::term::ansi::sgr::bg::black {} { return 40 }
proc lib::term::ansi::sgr::bg::red {} { return 41 }
proc lib::term::ansi::sgr::bg::green {} { return 42 }
proc lib::term::ansi::sgr::bg::yellow {} { return 43 }
proc lib::term::ansi::sgr::bg::blue {} { return 44 }
proc lib::term::ansi::sgr::bg::magenta {} { return 45 }
proc lib::term::ansi::sgr::bg::cyan {} { return 46 }
proc lib::term::ansi::sgr::bg::white {} { return 47 }
proc lib::term::ansi::sgr::bg::8-bit color { return [list 48 5 $color] }
proc lib::term::ansi::sgr::bg::rgb {r g b} { return [list 48 2 $r $g $b] }
proc lib::term::ansi::sgr::bg::default {} { return 49 }


proc lib::test {name args script arrow expected} {
    named-args {
        -constraints {myConstraints {}}
    }

    upvar stats stats
    upvar constraints constraints

    dict incr stats total
    foreach constr $myConstraints {
        if {$constr ni $constraints} {
            dict incr stats skipped
            return
        }
    }

    catch $script result

    set matched [switch -- $arrow {
        ->   { expr {$result eq $expected} }
        ->*  { string match $expected $result }
        ->$  { regexp -- $expected $result }
        default {
            return -code error \
                   -errorcode {JIMLIB TEST BAD-ARROW} \
                   [list unknown arrow: $arrow]
        }
    }]

    if {!$matched} {
        set error {}
        append error "\n>>>>> $name failed: [list $script]\n"
        append error "      got: [list $result]\n"
        append error " expected: [list $expected]"
        if {$arrow ne {->}} {
            append error "\n    match: $arrow"
        }

        dict incr stats failed

        puts stderr $error
    }

    dict incr stats passed
}


proc lib::try args {
    # These are not local, since this allows us to [uplevel] a [catch] rather
    # than [catch] the [uplevel]ing of something, resulting in a cleaner
    # -errorinfo:
    set EMVAR  ::tcl::control::em
    set OPTVAR ::tcl::control::opts
    upvar $EMVAR em
    upvar $OPTVAR opts

    set magicCodes { ok 0 error 1 return 2 break 3 continue 4 signal 5 }

    # ----- Parse arguments -----

    set trybody [lindex $args 0]
    set finallybody {}
    set handlers [list]
    set i 1

    while {$i < [llength $args]} {
        switch -- [lindex $args $i] {
            "on" {
                incr i
                set code [lindex $args $i]

                if {[dict exists $magicCodes $code]} {
                    set code [dict get $magicCodes $code]
                } elseif {![string is integer -strict $code]} {
                    set msgPart [join [dict keys $magicCodes] {", "}]
                    return -code error \
                           -errorcode {JIMLIB TRY BAD-CODE} \
                           "bad code '[lindex $args $i]': must be\
                            integer or \"$msgPart\""
                }

                lappend handlers \
                    [lrange $args $i $i] \
                    [format %d $code] \
                    {} \
                    {*}[lrange $args $i+1 $i+2]

                incr i 3
            }

            "trap" {
                incr i

                if {[catch {llength [lindex $args $i]}]} {
                    return -code error \
                           -errorcode {JIMLIB TRY BAD-PREFIX} \
                           "bad prefix '[lindex $args $i]':\
                            must be a list"
                }

                lappend handlers \
                    [lrange $args $i $i] \
                    1 \
                    {*}[lrange $args $i $i+2]

                incr i 3
            }

            "finally" {
                incr i
                set finallybody [lindex $args $i]

                incr i
                break
            }

            default {
                return -code error \
                       -errorcode {JIMLIB TRY BAD-HANDLER} \
                       "bad handler '[lindex $args $i]': must be\
                        \"on code varlist body\", or\
                        \"trap prefix varlist body\""
            }
        }
    }

    if {($i != [llength $args]) || ([lindex $handlers end] eq "-")} {
        return -code error \
               -errorcode {TCL WRONGARGS} \
               "wrong # args: should be\
                \"try body ?handler ...? ?finally body?\""
    }

    # ----- Execute 'try' body -----

    set code [uplevel 1 [list ::catch $trybody $EMVAR $OPTVAR]]

    # Keep track of the original error message & options
    set _em $em
    set _opts $opts
    # ----- Find and execute handler -----

    set errorcode {}
    if {[dict exists $opts -errorcode]} {
        set errorcode [dict get $opts -errorcode]
    }
    set found 0

    foreach {descrip oncode pattern varlist body} $handlers {
        if {!$found} {
            if {
                ($code != $oncode)
                || ([lrange $pattern 0 end] ne
                    [lrange $errorcode 0 [llength $pattern]-1])
            } {
                continue
            }
        }
        set found 1
        if {$body eq "-"} {
            continue
        }

        # Handler found ...

        # Assign trybody results into variables
        lassign $varlist resultsVarName optionsVarName
        if {[llength $varlist] >= 1} {
            upvar 1 $resultsVarName resultsvar
            set resultsvar $em
        }
        if {[llength $varlist] >= 2} {
            upvar 1 $optionsVarName optsvar
            set optsvar $opts
        }

        # Execute the handler
        set code [uplevel 1 [list ::catch $body $EMVAR $OPTVAR]]

        # Handler result replaces the original result (whether success or
        # failure); capture context of original exception for reference.
        set _em $em
        set _opts $opts

        # Handler has been executed - stop looking for more
        break
    }

    # No catch handler found -- error falls through to caller
    # OR catch handler executed -- result falls through to caller

    # ----- If we have a finally block then execute it -----

    if {$finallybody ne {}} {
        set code [uplevel 1 [list ::catch $finallybody $EMVAR $OPTVAR]]

        # Finally result takes precedence except on success

        if {$code != 0} {
            set _em $em
            set _opts $opts
        }

        # Otherwise our result is not affected
    }

    # Propagate the error or the result of the executed catch body to the
    # caller.
    dict incr _opts -level
    return -code $_opts(-code) \
           -level $_opts(-level) \
           $_em
}


proc lib::run-tests {} {
    set stats { total 0 passed 0 skipped 0 failed 0 }
    set constraints {}

    foreach cmd {gzip tar no-such-thing} {
        try {
            exec which $cmd
        } on ok _ {
            lappend constraints $cmd
        } on error _ {}
    }

    test file::split.1.1 {file::split /foo/bar/baz}     -> {/ foo bar baz}
    test file::split.1.2 {file::split /foo/bar/baz/}    -> {/ foo bar baz}
    test file::split.1.3 {file::split ///foo/bar/baz}   -> {/ foo bar baz}
    test file::split.1.4 {file::split foo/bar/baz}      -> {foo bar baz}
    test file::split.1.5 {file::split ./foo/bar/baz}    -> {. foo bar baz}
    test file::split.1.6 {file::split ../foo/bar/baz}   -> {.. foo bar baz}
    test file::split.1.7 {file::split .../foo/bar/baz}  -> {... foo bar baz}
    test file::split.1.8 {
        file::split тест/こんにちは/世界
    } -> {тест こんにちは 世界}

    test file::split.2.1 {file::split {}}       -> {}
    test file::split.2.2 {file::split {     }}  -> {{     }}
    test file::split.2.3 {file::split .}    -> .
    test file::split.2.4 {file::split ..}   -> ..
    test file::split.2.5 {file::split ...}  -> ...
    test file::split.2.6 {file::split /}    -> /
    test file::split.2.7 {file::split ///}  -> /

    test file::split.3.1 {
        file::split {   /foo/bar/baz      }
    } -> {{   } foo bar {baz      }}
    test file::split.3.2 {file::split { }}  -> {{ }}

    test file::split.4 {
        file::split 0/1/2/3/4/5/6/7/8/9/a/b/d/e/f/ 10
    } -> {aborted: path has too many parts}

    test file::split.5.1 {file::split /foo/~bar/baz}    -> {/ foo ./~bar baz}
    test file::split.5.2 {file::split ~foo}             -> ~foo
    test file::split.5.3 {file::split ~~~foo}           -> ~~~foo
    test file::split.5.4 {file::split /foo/~bar/baz}    -> {/ foo ./~bar baz}
    test file::split.5.5 {
        file::split /~foo/~bar/~baz
    }  -> {/ ./~foo ./~bar ./~baz}
    test file::split.5.6 {file::split /foo/~~~bar/baz}  -> {/ foo ./~~~bar baz}
    test file::split.5.7 {file::split ~foo/~bar/baz}    -> {~foo ./~bar baz}


    test string::bytefirst-1.1 {lib::string::bytefirst c abcdef} -> 2
    test string::bytefirst-1.2 {lib::string::bytefirst f abcdef} -> 5
    test string::bytefirst-1.3 {lib::string::bytefirst world helloworld} -> 5
    test string::bytefirst-1.4 {lib::string::bytefirst е тест} -> 2
    test string::bytefirst-1.5 {lib::string::bytefirst тест мегатест} -> 8


    test named-args-1.1 {
        set args {-foo 1 -bar 5}
        lib::named-args {
            -foo  foo
            -bar  bar
            -baz  {baz default}
        }
        return [list $foo $bar $baz]
    } -> {1 5 default}

    test named-args-1.2.1 {
        set args {-foo 1 -bar 5}
        lib::named-args {
            -foo  foo
            -bar  bar
            -baz  baz
        }
    } -> {missing required argument -baz}

    test named-args-1.2.2 {
        set args {-foo 1}
        lib::named-args {
            -foo  foo
            -bar  bar
            -baz  baz
        }
    } ->$ {missing required argument -ba. -ba.}

    test named-args-1.3.1 {
        set args {-foo 1 -bar 5 -qux wat}
        lib::named-args {
            -foo  foo
            -bar  bar
        } 1
    } -> {unknown extra arguments: -qux}

    test named-args-1.3.2 {
        set args {-foo 1 -bar 5 -qux wat}
        lib::named-args {
            -foo  foo
            -bar  bar
        }
    } -> {unknown extra arguments: {-qux wat}}

    test named-args-1.4 {
        set args {-foo a b c}
        list [lib::named-args {
            -foo  {foo {default foo} flag}
        } 1] $foo
    } -> {{a b c} {default foo}}

    test named-args-1.5 {
        set args {--foo xyzzy}
        lib::named-args {
            -f     -
            -foo   -
            --foo  var
        } 1
        set var
    } -> xyzzy


    cd [file dirname [info script]]

    test file::find-1.1 {
        lsort [file::find find-test-data]
    } -> "find-test-data/a/1\
          find-test-data/b/2\
          find-test-data/b/d/4\
          find-test-data/b/e/5\
          find-test-data/b/e/f/.6\
          find-test-data/b/e/f/g/7\
          find-test-data/c/.3"

    test file::find-1.2 {
        set found {}
        file::find find-test-data file depth {} {
            lappend found [list $file $depth]
            list
        }

        lsort -index 1 $found
    } -> "{find-test-data/a/1 2}\
          {find-test-data/b/2 2}\
          {find-test-data/c/.3 2}\
          {find-test-data/b/d/4 3}\
          {find-test-data/b/e/5 3}\
          {find-test-data/b/e/f/.6 4}\
          {find-test-data/b/e/f/g/7 5}"

    test file::find-1.3 {
        set found {}
        file::find find-test-data file depth {} {
            lappend found [list $file $depth]
            list
        } dir {[file readable $dir] && $depth < 2}

        lsort $found
    } -> {{find-test-data/a/1 2} {find-test-data/b/2 2} {find-test-data/c/.3 2}}

   test file::find-1.4 {
        set found [file::find find-test-data f d {
            [regexp {^[^/]+/./.$} $f]
        } {
            list $f $d
        }]

        lsort $found
    } -> {{find-test-data/a/1 2} {find-test-data/b/2 2}}

   test file::find-1.5 {
        set found [file::find find-test-data f d {} {
            file tail $f
        }]

        lsort $found
    } -> {.3 .6 1 2 4 5 7}


    set correctSeconds [clock scan {2014-06-26-20-10-00} \
            -format {%Y-%m-%d-%H-%M-%S}]
    set correctSecondsShort [clock scan {2014-01-01-00-00-00} \
            -format {%Y-%m-%d-%H-%M-%S}]

    test clock::scan-incremental-1.1 {
        clock::scan-incremental {2014-06-26 20:10}
    } -> [list $correctSeconds %Y-%m-%dT%H:%M]

    test clock::scan-incremental-1.2 {
        clock::scan-incremental {2014-06-26T20:10}
    } -> [list $correctSeconds %Y-%m-%dT%H:%M]

    test clock::scan-incremental-1.3 {
        clock::scan-incremental {2014 06 26 20 10}
    } -> [list $correctSeconds {%Y-%m-%dT%H:%M}]

    test clock::scan-incremental-1.4 {
        clock::scan-incremental {2014/06/26 20:10}
    } -> [list $correctSeconds {%Y-%m-%dT%H:%M}]

    test clock::scan-incremental-2.1 {
        clock::scan-incremental 2014
    } -> [list $correctSecondsShort %Y]

    test clock::scan-incremental-2.2 {
        clock::scan-incremental 2014-01
    } -> [list $correctSecondsShort %Y-%m]

    test clock::scan-incremental-2.3 {
        clock::scan-incremental {2014-01-01 00:00:00}
    } -> [list $correctSecondsShort {%Y-%m-%dT%H:%M:%S}]


    test clock::add-1.1 {
        clock::add 0 5 seconds
    } -> 5

    test clock::add-1.2 {
        clock::add 1 5 min
    } -> 301

    test clock::add-1.3 {
        clock::add 1 1 hour
    } -> 3601

    test clock::add-1.4 {
        clock::add 57 1 day
    } -> 86457

    test clock::add-1.5 {
        clock::add 0 1 day -12 hours -660 minutes -59 minutes -59 seconds
    } -> 1

    test clock::add-1.6 {
        clock::add 536526000 1 wk
    } -> 537130800

    test clock::add-1.7 {
        clock::add 536526000 4 wks
    } -> 538945200

    test clock::add-2.1 {
        expr {[clock::add 1575222308 1 month] - 1577900713 < 60}
    } -> 1

    test clock::add-2.2 {
        expr {[clock::add 1575222308 3 months] - 1583084747 < 60}
    } -> 1

    test clock::add-2.3 {
        clock::add 536526000 6 months
    } -> 552164400

    test clock::add-2.4 {
        clock::add 536526000 1 year
    } -> 568062000

    test clock::add-2.5 {
        clock::add 1000000000 10 years
    } -> 1315532800

    test clock::add-2.6 {
        clock::add -2208963600 100 years
    } -> 946710000


    test set::sym-diff-1.1 {
        set::sym-diff {a b c} {}
    } -> {{a b c} {}}

    test set::sym-diff-1.2 {
        set::sym-diff {} {a b c}
    } -> {{a b c} {}}

    test set::sym-diff-1.3 {
        set::sym-diff {a b c d e} {b c}
    } -> {{a d e} {b c}}

    test set::sym-diff-1.4 {
        set::sym-diff {a b c} {a b c}
    } -> {{} {a b c}}

    test set::sym-diff-1.5 {
        set::sym-diff {} {}
    } -> {{} {}}


    test set::subset?-1.1 {set::subset? {a b c} {a b c}} -> 1
    test set::subset?-1.2 {set::subset? {} {}} -> 1
    test set::subset?-1.3 {set::subset? {a b c} {}} -> 0
    test set::subset?-1.4 {set::subset? {} {a b c}} -> 1
    test set::subset?-1.5 {set::subset? {b e} {a b c d e f}} -> 1
    test set::subset?-1.6 {set::subset? {a x b} {a b c d e f}} -> 0


    test set::diff-1.1 {set::diff {a b c} {a b c}} -> {}
    test set::diff-1.2 {set::diff {} {}} -> {}
    test set::diff-1.3 {set::diff {a b c} {}} -> {a b c}
    test set::diff-1.4 {set::diff {} {a b c}} -> {}
    test set::diff-1.5 {set::diff {b e} {a b c d e f}} -> {}
    test set::diff-1.6 {set::diff {a x b} {a b c d e f}} -> x
    test set::diff-1.7 {set::diff {a b c d e f} {b e}} -> {a c d f}


    test log-1.1 {log discard hello} -> {}
    test log-1.2 {log info hello -configure {level err}} ->* {wrong # args*}
    test log-1.3 {
        set f [file tempfile]
        defer {file delete $f}

        set ch [open $f w]
        variable log::config
        set config [dict merge $config [list channel $ch level debug]]
        log debug test
        log debug {test 2}
        close $ch

        file::read $f
    } ->* {==*(debug) test*==*(debug) {test 2}*}


    test pipe-1.1 {
        pipe x {foo bar baz} {lindex $x 1}
    } -> bar

    test pipe-1.2 {
        pipe x {foo bar baz} -each {lindex $x 0}
    } -> {foo bar baz}

    test pipe-1.3 {
        pipe x {foo bar baz} -each {string reverse $x}
    } -> {oof rab zab}

    test pipe-1.4 {
        pipe x [range 0 101] -reduce acc 0 {expr $x + $acc}
    } -> 5050

    test pipe-1.5 {
        pipe x {foo bar baz} -flatMap {split $x {}}
    } -> {f o o b a r b a z}

    test pipe-1.6 {
        pipe x {0 0 5 0 0 1 1 1 1 -99 108} -filter {$x > 0}
    } -> {5 1 1 1 1 108}

    test pipe-1.7 {
        pipe x {1foo foo1 1bar bar1 {} 111bar11 foo} -filter {[string match *foo* $x]}
    } -> {1foo foo1 foo}

    test pipe-1.8 {
        pipe x {1x 2x 3x xx 4x 5x yy 6x 7x} -map {
            if {![regexp (\\d) $x _ n]} continue
            lindex $n
        }
    } -> {1 2 3 4 5 6 7}

    test pipe-1.9 {
        pipe x {foo bar baz ENOUGH junk 23o432} -map {
            if {$x eq {ENOUGH}} break
            lindex $x
        }
    } -> {foo bar baz}

    test pipe-2.1 {
        pipe _ 6 {
            range 1 $_
        } -map {
            string repeat # $_
        } -reduce s {} {
            append s $_\n
        }
    } -> #\n##\n###\n####\n#####\n


    test string::indent-1.1 {
        string::indent 1\n2\n3 {    }
    } -> "    1\n    2\n    3"

    test string::indent-1.2 {
        string::indent 1\n2\n3\n \t
    } -> \t1\n\t2\n\t3\n\t


    test template::eval-1.1 {
        template::eval {A<%= $foo %>C} foo B
    } -> ABC

    test template::eval-1.2 {
        set a 7
        template::eval {<% upvar 1 a a %><%= $a %>}
    } -> 7


    test file::reroot-1.1 {file::reroot /foo /xxx /foo/bar/baz} -> /xxx/bar/baz
    test file::reroot-1.2 {file::reroot /foo/bar /xxx /foo/bar/baz} -> /xxx/baz
    test file::reroot-1.3 {file::reroot /foo/bar/baz /xxx /foo/bar/baz} -> /xxx
    test file::reroot-1.4 {file::reroot / {} /foo/bar/baz} -> foo/bar/baz
    test file::reroot-1.5 {file::reroot foo xxx foo/bar/baz} -> xxx/bar/baz
    test file::reroot-1.6 {file::reroot foo/bar xxx foo/bar/baz} -> xxx/baz
    test file::reroot-1.7 {file::reroot foo/bar/baz xxx foo/bar/baz} -> xxx
    test file::reroot-1.8 {
        file::reroot /nope / /foo/bar/baz
    } -> {/foo/bar/baz not in /nope}


    test file::with-path-1.1 {
        file::with-path find-test-data/b/ {
            file isfile 2
        }
    } -> 1

    test file::with-path-1.2 {
        file::with-path find-test-data/b/ {}
        file isdir find-test-data
    } -> 1


    test file::list-dir-1.1 {
        file::list-dir find-test-data/
    } -> {find-test-data/a find-test-data/b find-test-data/c}
    test file::list-dir-1.2 {
        file::list-dir find-test-data/c/
    } -> find-test-data/c/.3
    test file::list-dir-1.3 {
        file::list-dir find-test-data/b/e/f/
    } -> {find-test-data/b/e/f/.6 find-test-data/b/e/f/g}

    test file::list-dir-2.1 {
        file::list-dir -tails find-test-data/
    } -> {a b c}
    test file::list-dir-2.2 {
        file::list-dir -tails find-test-data/c/
    } -> .3
    test file::list-dir-2.3 {
        file::list-dir -tails find-test-data/b/e/f/
    } -> {.6 g}

    test file::list-dir-3.1 {
        file mkdir empty/
        defer {file delete empty/}
        file::list-dir empty
    } -> {}


    test try-1.1 {try {lindex foo}} -> foo
    test try-1.2 {try {lindex foo} on ok _ {lindex bar}} -> bar
    test try-1.3 {try {lindex foo} on ok x {lindex $x}} -> foo
    test try-1.4 {try {lindex foo} on error _ {lindex bar}} -> foo
    test try-1.5 {try {lindex foo} on return _ {lindex bar}} -> foo
    test try-1.6 {try {lindex foo} on break _ {lindex bar}} -> foo
    test try-1.7 {try {lindex foo} on continue _ {lindex bar}} -> foo
    test try-1.8 {try {lindex foo} on # _ {lindex bar}} ->* {bad code '#'*}

    test try-2.1 {try {error nope} on error _ {lindex bar}} -> bar
    test try-2.2 {try {error nope} on break _ {lindex bar}} -> nope
    test try-2.3 {try break on break _ {lindex bar}} -> bar
    test try-2.4 {try continue on break _ {lindex bar}} -> {}
    test try-2.5 {try continue on continue _ {lindex bar}} -> bar
    test try-2.6 {try {return nope} on continue _ {lindex bar}} -> nope
    test try-2.7 {try {return nope} on return _ {lindex bar}} -> bar

    test try-3.1 {
        try {
            apply {{} {return -code error -errorcode {FOO BAR} failed}}
        } trap FOO {msg opts} {
            list trapped
        }
    } -> trapped

    test try-3.2 {
        try {
            apply {{} {return -code error -errorcode {FOO BAR} failed}}
        } trap {FOO BAR} {msg opts} {
            list trapped
        }
    } -> trapped

    test try-3.3 {
        try {
            apply {{} {return -code error -errorcode {FOO BAR} failed}}
        } trap BAZ {msg opts} {
            list trapped
        }
    } -> failed

    test try-3.4 {
        try {
            apply {{} {return -code error -errorcode {FOO BAR} failed}}
        } trap {FOO BAZ} {msg opts} {
            list trapped
        }
    } -> failed


    test list::lcs-1.1 {list::lcs {a b c} {a b c}} -> {{0 1 2} {0 1 2}}
    test list::lcs-1.2 {list::lcs {a b c} {a c b}} -> {{0 1} {0 2}}
    test list::lcs-1.3 {
        list::lcs {1 h e l l o 3 0} {2 w o r l d 4 5 6 0}
    } -> {{3 7} {4 9}}
    test list::lcs-1.4 {
        set list1 {b c d e f g h}
        set list2 {a c d g i j k}
        lassign [list::lcs $list1 $list2] indices1 indices2
        list [list::pick $list1 $indices1] [list::pick $list2 $indices2]
    } -> {{c d g} {c d g}}

    test list::lcs-2.1 {list::lcs {} {a c b}} -> {{} {}}
    test list::lcs-2.2 {list::lcs {a b c} {}} -> {{} {}}
    test list::lcs-2.3 {list::lcs {} {}} -> {{} {}}


    test list::pick-1.1 {list::pick {a b c d e f g h} {0 0 0 5}} -> {a a a f}
    test list::pick-1.2 {list::pick {a b c d e f g h} {}} -> {}


    test list::diff-1.1 {list::diff {} {}} -> {}
    test list::diff-1.2 {list::diff foo {}} -> {- foo}
    test list::diff-1.3 {list::diff {} bar} -> {+ bar}
    test list::diff-1.4 {list::diff foo bar} -> {- foo + bar}

    test list::diff-1.5 {
        list::diff {bar baz} {foo bar baz}
    } -> {+ foo = bar = baz}

    test list::diff-1.6 {
        list::diff {foo baz} {foo bar baz}
    } -> {= foo + bar = baz}

    test list::diff-1.7 {
        list::diff {foo bar} {foo bar baz}
    } -> {= foo = bar + baz}

    test list::diff-1.8 {
        list::diff {a c f} {a b c d e f}
    } -> {= a + b = c + d + e = f}

    test list::diff-1.9 {
        list::diff {a b c d e f} {a c f}
    } -> {= a - b = c - d - e = f}

    test list::diff-1.10 {
        list::diff {0 1 2 3} {a b c 0 e f g}
    } -> {+ a + b + c = 0 - 1 - 2 - 3 + e + f + g}


    test file::tar::header-1.1 {
        file::tar::header -filename hello.txt -size 42
    } ->* hello.txt*

    test file::tar::header-2.1 {
        file::tar::header -filename [string repeat X 100] -size 0
    } -> {field filename exceeds 99 bytes}

    test file::tar::write-1.1 -constraints tar {
        set f [file tempfile]
        defer {file delete $f}

        set ch [open $f w]
        file::tar::write $ch {Hello, world!} \
                             -filename {dir/local greeting.txt} \
                             -mtime 1576740678
        file::tar::write $ch {Hello, universe! ... ... ... Aargh!} \
                             -filename dir/careful.txt \
                             -uname foo \
                             -gname bar \
                             -mtime 0
        file::tar::finish $ch
        close $ch

        exec tar tvf $f
    } ->* "-rw-r--r--*root*root*13*2019-12-*dir/local\
           greeting.txt*-rw-r--r--*foo*bar*35*1970-01-*dir/careful.txt*"

    test file::tar::write-2.1 -constraints {gzip tar} {
        set f [file tempfile]
        defer {file delete $f}

        set ch [open [list | gzip > $f] w]
        file::tar::write $ch {Hello, world!} \
                             -filename hello.txt \
                             -mtime 1576740678
        file::tar::finish $ch
        close $ch

        exec tar ztvf $f
    } ->* *root*root*hello.txt*


    test term::ansi::sgr-1.1 {
        term::ansi::sgr
    } -> \033\[m

    test term::ansi::sgr-1.2 {
        term::ansi::sgr {bg black} \
                        {fg green} \
                        bold \
                        italic \
                        {underline double} \
                        strikethrough
    } -> \033\[40\;32\;1\;3\;21\;9m

    test term::ansi::sgr-1.3 {
        term::ansi::sgr {fg 8-bit 42} {bg rgb 1 2 3}
    } -> \033\[38\;5\;42\;48\;2\;1\;2\;3m

    test term::ansi::sgr::rgb5-to-8-bit-1.1 {
        list [term::ansi::sgr::rgb5-to-8-bit 5 0 0] \
             [term::ansi::sgr::rgb5-to-8-bit 0 5 0] \
             [term::ansi::sgr::rgb5-to-8-bit 0 0 5] \
             [term::ansi::sgr::rgb5-to-8-bit 5 5 0] \
             [term::ansi::sgr::rgb5-to-8-bit 0 5 5] \
             [term::ansi::sgr::rgb5-to-8-bit 5 0 5]
    } -> {196 46 21 226 51 201}

    test term::ansi::sgr::rgb5-to-8-bit-2.1 {
        term::ansi::sgr::rgb5-to-8-bit 0 6 0
    } -> {rgb values {0 6 0} not in range of 0 to 5}


    test list::sl-1.1 {
        list::sl {1 2 3 $(2 + 2) [lindex 5]}
    } -> {1 2 3 4 5}

    test list::sl-1.2 {
        list::sl {
            foo  ;# bar
            baz
        }
    } -> {foo baz}

    test list::sl-2.1 {
        list::sl foo {}
    } -> {invalid command name "foo"}

    test list::sl-2.2 {
        set i 0
        list::sl {
            incr i  ;# foo
            incr i
            incr i
        } {}
    } -> {1 2 3}


    test mp::pmap-1.1 {
        mp::pmap x [range 0 100] {lindex $x} 20
    } -> [range 0 100]

    test mp::pmap-1.2 {
        mp::pmap {x y} [range 0 10] {expr {$x + $y}} 2
    } -> {1 5 9 13 17}

    test mp::pmap-2.1 {
        mp::pmap x [range 0 100] {lindx $x} 20
    } ->$ [string cat {child process \d+ for chunk 0 to 19 } \
                      {exited with status 1 and message } \
                      {\{invalid command name "lindx"\}}]


    test csv::split-1.1 {csv::split 1,2,3,4,5} -> {1 2 3 4 5}
    test csv::split-1.2 {csv::split "1","2","3","4","5"} -> {1 2 3 4 5}
    test csv::split-1.3 {csv::split "aaa","b""bb","ccc"} -> {aaa {b"bb} ccc}
    test csv::split-1.4 {
        csv::split {"Hello, world!",37,foo}
    } -> {{Hello, world!} 37 foo}
    test csv::split-1.5 {
        csv::split "aaa","b\n\n\nbb","ccc"
    } -> "aaa {b\n\n\nbb} ccc"
    # How the following case should be handled is ambiguous.  The test is
    # documentation, not a prescription.  {1 { "2"} {"3" } { "4" } {   "5"  }}
    # (i.e., ignoring double quotes when there aren't two of them adjacent to
    # the separators around the value) would also be a reasonable result.
    test csv::split-1.6 {
        csv::split {"1", "2","3" , "4" ,   "5"  }
    } -> {1 { 2} {3 } { 4 } {   5  }}
    test csv::split-1.7 {csv::split 1,,,,5} -> {1 {} {} {} 5}
    test csv::split-1.8 {csv::split {",",",",","}} -> {, , ,}
    test csv::split-1.9 {csv::split {}} -> {}
    test csv::split-1.10 {csv::split {   }} -> {{   }}

    test csv::split-2.1 {csv::split foo\t\tbar\tbaz {\t}} -> {foo {} bar baz}


    test hash::mult-1.1 {
        format %x [hash::mult a000]
    } -> 17c9312d6
    test hash::mult-1.2 {
        format %x [hash::mult a000 0 31]
    } -> 2cd22f
    test hash::mult-1.3 {
        format %x $([hash::mult {Hello, world!}] & 0xffffffff)
    } -> e18796ae
    test hash::mult-1.4 {
        format %x $([hash::mult {Hello, world!} 0 31] & 0xffffffff)
    } -> 8ff0cbf5
    test hash::mult-1.5 {
        hash::mult {why so negative, huh?}
    } -> -2972932130062078


    test checksum::parse::line-1.1 {
        checksum::parse::line \
            {b40e0f641d786c71a48fc36476f2d3c8  jimlib.tcl}
    } -> {? jimlib.tcl b40e0f641d786c71a48fc36476f2d3c8}

    test checksum::parse::line-1.2 {
        checksum::parse::line \
            "b40e0f641d786c71a48fc36476f2d3c8 *jimlib.tcl"
    } -> {? jimlib.tcl b40e0f641d786c71a48fc36476f2d3c8}

    test checksum::parse::line-1.3 {
        checksum::parse::line \
            "B40E0F641D786C71A48FC36476F2D3C8   \t  jimlib.tcl"
    } -> {? jimlib.tcl B40E0F641D786C71A48FC36476F2D3C8}

    test checksum::parse::line-1.4 {
        checksum::parse::line \
            {\d41d8cd98f00b204e9800998ecf8427e  a\nb}
    } -> "? {a\nb} d41d8cd98f00b204e9800998ecf8427e"

    test checksum::parse::line-2.1 {
        checksum::parse::line blah
    } -> {can't parse line blah}

    test checksum::parse::tagged-line-1.1 {
        checksum::parse::tagged-line \
            "SHA3-224 (jimlib.tcl) =\
             2727b06b1fab7cec39d0d95596e2b7486207740541c09c8c72c972f3"
    } -> "SHA3-224\
          jimlib.tcl\
          2727b06b1fab7cec39d0d95596e2b7486207740541c09c8c72c972f3"

    test checksum::parse::tagged-line-1.2 {
        checksum::parse::tagged-line \
            "SHA3-224   (jimlib.tcl)  =  \t  \
             2727B06B1FAB7CEC39D0D95596E2B7486207740541C09C8C72C972F3"
    } -> "SHA3-224\
          jimlib.tcl\
          2727B06B1FAB7CEC39D0D95596E2B7486207740541C09C8C72C972F3"

    test checksum::parse::tagged-line-1.3 {
        checksum::parse::tagged-line \
            {\MD5 (a\nb) = d41d8cd98f00b204e9800998ecf8427e}
    } -> "MD5 {a\nb} d41d8cd98f00b204e9800998ecf8427e"

    test checksum::parse::tagged-line-1.4 {
        checksum::parse::tagged-line \
            {MD5 (Image(927).jpg) = 2f008c8309322d7ef872814746f33979}
    } -> {MD5 Image(927).jpg 2f008c8309322d7ef872814746f33979}

    test checksum::parse::tagged-line-1.5 {
        checksum::parse::tagged-line \
            {MD5 (abc(def) = 2f008c8309322d7ef872814746f33979}
    } -> {MD5 abc(def 2f008c8309322d7ef872814746f33979}

    test checksum::parse::tagged-line-1.6 {
        checksum::parse::tagged-line \
            {MD5 (abcdef) = 2f00) = 2f008c8309322d7ef872814746f33979}
    } -> {MD5 {abcdef) = 2f00} 2f008c8309322d7ef872814746f33979}

    test checksum::parse::tagged-line-2.1 {
        checksum::parse::tagged-line blah
    } -> {can't parse line blah}

    test checksum::parse::many-1.1 {
        checksum::parse::many [join {
            {MD5 (Makefile) = 8da0c2013b7e5c4df41acc5dc55b4166}
            {MD5 (procs.tcl) = 39f17dd5517a0e68de0bc095164d8607}
            {ce6a6bb38843f66320dc04b094554d68  README.md}
        } \n]
    } -> "MD5 Makefile 8da0c2013b7e5c4df41acc5dc55b4166\
          MD5 procs.tcl 39f17dd5517a0e68de0bc095164d8607\
          ? README.md ce6a6bb38843f66320dc04b094554d68"

    test checksum::parse::many-2.1 {
        checksum::parse::many \n\t\n1\n2\n3\n
    } -> {can't parse line 1}


    test math::atan2-1.1 {math::atan2 1.0 2.0}   ->* 0.463647*
    test math::atan2-1.2 {math::atan2 0.0 1.0}   ->  0.0
    test math::atan2-1.3 {math::atan2 1.0 0.0}   ->* 1.570796*
    test math::atan2-1.4 {math::atan2 0.0 0.0}   ->  0.0
    test math::atan2-1.5 {math::atan2 0.25 0.75} ->* 0.321750*


    test math::complex::+-1.1 {math::complex::+ {1 -2} {5 7}} -> {6 5}
    test math::complex::+-1.2 {math::complex::+ 1 2} -> {3 0}

    test math::complex::*-1.1 {math::complex::* 1 1} -> {1 0}
    test math::complex::*-1.2 {math::complex::* {0 1} {0 1}} -> {-1 0}
    test math::complex::*-1.3 {math::complex::* {2 3} {7 11}} -> {-19 43}

    test math::complex::mod-1.1 {math::complex::mod 1} -> 1.0
    test math::complex::mod-1.2 {math::complex::mod {0 1}} -> 1.0
    test math::complex::mod-1.3 {math::complex::mod {2 2}} ->* 2.828427*


    test string::match-bracket-1.1 {string::match-bracket ()()()} -> 1
    test string::match-bracket-1.2 {string::match-bracket {(1 + -2 * (3 + 4))}} -> 17
    test string::match-bracket-1.3 {string::match-bracket -start 3 {abs(1 + -2 * (3 + 4))}} -> 20
    test string::match-bracket-1.4 {string::match-bracket {(\()}} -> 3
    test string::match-bracket-1.5 {string::match-bracket -escape ^ {(^()}} -> 3

    test string::match-bracket-2.1 {string::match-bracket (} -> {unmatched brackets: 1}
    test string::match-bracket-2.2 {string::match-bracket (((} -> {unmatched brackets: 3}
    test string::match-bracket-2.3 {string::match-bracket ((((())))} -> {unmatched brackets: 1}
    test string::match-bracket-2.4 {string::match-bracket {}} -> {start isn't a bracket}
    test string::match-bracket-2.5 {string::match-bracket { ()()()}} -> {start isn't a bracket}


    test string::slugify-1.1 {string::slugify hello-world} -> hello-world
    test string::slugify-1.2 {string::slugify {Hello, world!!}} -> hello-world


    test $-1.1 {set a 5; $ a} -> 5
    test $-1.2 {set a {k1 v1 k2 v2}; $ a} -> {k1 v1 k2 v2}
    test $-1.3 {set a {k1 v1 k2 v2}; $ a k1} -> v1

    test $-2.1 {$ a 5} -> {can't read "a": no such variable}
    test $-2.2 {set a {k1 v1}; $ a k2} -> {key "k2" not known in dictionary}


    test id-1.1 {id foo} -> foo
    test id-2.1 {id foo bar} -> {wrong # args: should be "id value"}


    test fill-repeats-1.1 {dict::fill-repeats {a b c d}} -> {a b c d}
    test fill-repeats-1.2 {dict::fill-repeats {a - c - e f}} -> {a f c f e f}
    test fill-repeats-1.3 {dict::fill-repeats {a - c - e -}} -> {a {} c {} e {}}

    test fill-repeats-2.1 {dict::fill-repeats no} -> {not a dictionary: no}


    return $stats
}
