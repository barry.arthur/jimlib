# Jimlib

![GitLab CI pipeline](https://gitlab.com/dbohdan/jimlib/badges/master/pipeline.svg)

Jimlib is my personal standard library for for [Jim Tcl](http://jim.tcl-lang.org/).  It contains replacements for commands from Tcl 8.6 and Tcllib like `lib::clock::add` and `lib::try` and original functionality like `lib::named-args` and `lib::pipe` (a personal favorite).  It is oriented towards using Jim Tcl for shell scripting.  Most of the commands depend on few or no others; some work in mainline Tcl and some do not.

Jimlib is not affiliated with the Jim Tcl project.  The API is unstable and may change at any time.

## License

MIT, Tcl, and BSD.  See the top of `jimlib.tcl`.
