FORCE_COLOR := 0
JIMSH := jimsh
JIMLIB := jimlib.tcl

all: jim
test: jim

jim:
	$(JIMSH) -e 'source {*}$$argv; lib::run-tests' $(JIMLIB)

commands:
	$(JIMSH) -e 'source procs.tcl; procs-file $$palette {*}$$argv' $(JIMLIB) $(FORCE_COLOR)

commands-styles:
	$(JIMSH) -e 'source procs.tcl; styles $$palette'


.PHONY: all commands commands-styles jim test
