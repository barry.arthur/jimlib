#! /usr/bin/env jimsh
source [env JIMLIB [file dirname [info script]]/jimlib.tcl]

set palette [lmap n {
    74 110 117 153 195 231
    136 172 214 220 222 229
    33 69 105 141 177 213
} { list fg 8-bit $n }]


proc style-ns-path {palette nsPath} {
    set color -1
    lib::pipe x $nsPath {
        lib::string::sepsplit $x :: 0
    } {
        set parts {}
        foreach word $x {
            lappend parts [style-word $palette $word]
        }
        set parts
    } {
        join $x [sgr faint]::[sgr]
    }
}


proc style-word {palette word} {
    set style {}

    if {$word eq {black}} {
        lappend style {fg 8-bit 238}
    } elseif {$word in {blue cyan default green magenta red white yellow}} {
        lappend style [list fg $word]
    } elseif {$word in {
        bold
        faint
        italic
        underline
        blink
        reverse
        strikethrough
        overline
    }} {
        lappend style {fg white} $word
    } else {
        lappend style [lindex $palette [expr {
            [lib::hash::mult $word 3] % [llength $palette]
        }]]
    }

    return [sgr {*}$style]$word[sgr]
}


proc procs {palette script {forceColor 0}} {
    if {$forceColor || [stdout isatty]} {
        local proc sgr args {lib::term::ansi::sgr {*}$args}
    } else {
        local proc sgr args {}
    }

    set lines {}
    foreach cmd [lib::commands $script] {
        if {[lindex $cmd 0] ne {proc}} continue

        set line {}
        append line "[sgr faint]proc[sgr] "
        append line "[style-ns-path $palette [lindex $cmd 1]][sgr] "
        append line [lrange $cmd 2 2]
        if {[llength $cmd] == 5} {
            append line " [sgr faint][lindex $cmd 3][sgr] "
        }

        lappend lines $line
    }

    return [join $lines \n]
}


proc procs-file {palette file args} {
    tailcall procs $palette [lib::file::read $file] {*}$args
}


proc styles palette {
    set lines {}

    foreach style $palette {
        lappend lines "[lib::term::ansi::sgr $style]\u2588\u2588 ##\
                       [format %-12s $style]\
                       ## \u2588\u2588[lib::term::ansi::sgr]"
    }

    join $lines \n
}
